#FROM registry.access.redhat.com/rhel7:latest
FROM centos:latest

# https://caddyserver.com/download/linux/amd64?plugins=http.ipfilter,http.prometheus,http.proxyprotocol,http.ratelimit,http.realip,http.upload&license=personal
#plugins=http.ratelimit,http.upload,http.jwt,http.restic \

ENV fredl=2015 \
    GOPATH=/go/path \
    GOROOT=/usr/bin/go \
    GO111MODULE=on
ADD containerfiles/ /

RUN set -x \
  && yum -y install gunzip tar git \
  && curl --silent --show-error --fail --location \
  https://storage.googleapis.com/golang/go1.12.4.linux-amd64.tar.gz \
  | tar --no-same-owner -C /usr/bin/ -xzvf - \
  && export PATH=${GOROOT}/bin:${PATH} \
  && mkdir -p ${GOPATH} /go/caddybuild \
  && cd /go/caddybuild \
  && go mod init caddy \
  && go get github.com/mholt/caddy/caddy \
  && go build \
  && cp $GOPATH/bin/caddy /usr/bin/caddy \
  && rm -rf ${GOPATH} ${GOROOT} \
  && yum -y autoremove git \
  && yum -y clean all \
  && /usr/bin/caddy -version \
  && /usr/bin/caddy -plugins \
  && mkdir /docroots /docroots/upload \
  && chmod -R 777 /docroots /docroots/upload

# want to remove systemd this does not work
# && yum -y autoremove gunzip tar \
EXPOSE 2015

# ADD containerfiles/ /

WORKDIR /docroots/upload

ENTRYPOINT ["/usr/bin/caddy"]
CMD ["--conf", "/etc/caddy/caddyfile"]

# ENTRYPOINT ["strace"]
# CMD ["-fveall","-a1024","-s1024","-o","/tmp/caddy-strace","/usr/bin/caddy","--conf","/etc/caddy/caddyfile"]
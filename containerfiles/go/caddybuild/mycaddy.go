package main

import (
	"github.com/mholt/caddy/caddy/caddymain"
	
	// plug in plugins here, for example:
	// _ "import/path/here"

	_ "blitznote.com/src/caddy.upload"
    _ "github.com/BTBurke/caddy-jwt"
    _ "github.com/Xumeiquer/nobots"
    _ "github.com/abiosoft/caddy-git"
    _ "github.com/caddyserver/dnsproviders/azure"
    _ "github.com/caddyserver/dnsproviders/cloudflare"
    _ "github.com/caddyserver/dnsproviders/digitalocean"
    _ "github.com/caddyserver/dnsproviders/dnsmadeeasy"
    _ "github.com/caddyserver/dnsproviders/dyn"
    _ "github.com/caddyserver/dnsproviders/godaddy"
    _ "github.com/caddyserver/dnsproviders/googlecloud"
    _ "github.com/caddyserver/dnsproviders/linode"
    _ "github.com/caddyserver/dnsproviders/namecheap"
    _ "github.com/caddyserver/dnsproviders/ns1"
    _ "github.com/caddyserver/dnsproviders/ovh"
    _ "github.com/caddyserver/dnsproviders/pdns"
    _ "github.com/caddyserver/dnsproviders/rackspace"
    _ "github.com/caddyserver/dnsproviders/route53"
    _ "github.com/caddyserver/dnsproviders/vultr"
    _ "github.com/restic/caddy"
    _ "github.com/caddyserver/forwardproxy"
    _ "github.com/captncraig/caddy-realip"
    _ "github.com/captncraig/cors/caddy"
    _ "github.com/casbin/caddy-authz"
    _ "github.com/echocat/caddy-filter"
    _ "github.com/epicagency/caddy-expires"
    _ "github.com/freman/caddy-reauth"
    _ "github.com/hacdias/caddy-minify"
    _ "github.com/hacdias/caddy-webdav"
    _ "github.com/hiphref/caddy-geoip"
    _ "github.com/mastercactapus/caddy-proxyprotocol"
    _ "github.com/miekg/caddy-prometheus"
    _ "github.com/pyed/ipfilter"
    _ "github.com/simia-tech/caddy-locale"
    _ "github.com/tarent/loginsrv/caddy"
    _ "github.com/techknowlogick/caddy-s3browser"
    _ "github.com/xuqingfeng/caddy-rate-limit"
)

func main() {
	// optional: disable telemetry
	caddymain.EnableTelemetry = false
	caddymain.Run()
}
# what's

This repo use the caddy template module https://caddyserver.com/docs/templates 
to show some ENV Vars https://caddyserver.com/docs/template-actions

The **cd's**are relative to the `git clone` path

# Openshift example

oc new-project 1-caddy-teplate  
oc new-app https://gitlab.com/aleks001/caddy-template-usage.git   
oc expose service caddy-template-usage  
or  
oc create route edge --service=caddy-template-usage

## caddyfile

The config file for the caddyserver.

### add caddyfile

If you want to change something in the caddyfile you can use config maps instead
to build the whole image again.

```
# cd containerfiles/etc/caddy/
# oc create configmap caddyfile --from-file=caddyfile
# oc volumes dc/caddy-template-usage \
    --add \
    --mount-path=/etc/caddy \
    --source='{"configMap": { "name": "caddyfile"}}'
```

In newer openshift version is the syntax for `oc volume ..` now
```
# oc set volumes dc/caddy-template-usage \
    --add \
    --configmap-name="haproxy-template" \
    --mount-path=/mnt \
    --name="haproxy-template" \
    --type=configmap
```



### update caddyfile

```
$ oc delete configmap caddyfile && sleep 2 && \
    oc create configmap caddyfile --from-file=caddyfile
```

## template files

### add template files

```
$ cd containerfiles/docroots/templates/
$ oc create configmap env-template --from-file=.
$ oc volumes dc/caddy-template-usage --add --mount-path=/docroots/templates \
    --source='{"configMap": { "name": "env-template"}'
```

### add changed template files

when you change something in the templates you will need to replace the old one
with the new one

```
$ oc delete configmap env-template && sleep 2 && \
    oc create configmap env-template --from-file=.
```

# Files

print-env.tmpl:      Print the whole env from Container  
print-headers.tmpl:  Print some headers  
print-hostname.tmpl: Print only the hostname/container-name  